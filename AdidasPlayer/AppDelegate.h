//
//  AppDelegate.h
//  AdidasPlayer
//
//  Created by Radek Zmeskal on 22/07/15.
//  Copyright (c) 2015 Radek Zmeskal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

