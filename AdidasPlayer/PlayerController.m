//
//  PlayerController.m
//  AdidasPlayer
//
//  Created by Radek Zmeskal on 22/07/15.
//  Copyright (c) 2015 Radek Zmeskal. All rights reserved.
//

#import "PlayerController.h"

@interface PlayerController ()

@end

@implementation PlayerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadVideos];
    
    index = 0;
    AVPlayer *player = (AVPlayer*)[playlist objectAtIndex:index];
    
//    [player seekToTime:kCMTimeZero];
    [self setPlayer:player];
    [self setShowsPlaybackControls:NO];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(appEnteredForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[self player] play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)appEnteredForeground:(id) seder
{
//    AVPlayer *player = [playlist objectAtIndex:index];
//    
//    [self setPlayer:player];
    [[self player] play];
}

-(void)itemDidFinishPlaying:(NSNotification *) notification
{
    index++;
    if (index >= [playlist count])
    {
        index = 0;
    }
    
    AVPlayer *player = [playlist objectAtIndex:index];
    
    [player seekToTime:kCMTimeZero];
    [self setPlayer:player];
    [[self player] play];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)loadVideos
{
    playlist = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    NSString* path;
    NSURL *file;
    AVPlayerItem *item;
    AVPlayer *player;
    
    //old 1
    path = [[NSBundle mainBundle] pathForResource:@"Adias Ace Manchester United without players with music" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //old 2
    path = [[NSBundle mainBundle] pathForResource:@"Adidas X Manchester United without player with music" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //1
    path = [[NSBundle mainBundle] pathForResource:@"Blind_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];

    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //2
    path = [[NSBundle mainBundle] pathForResource:@"DiMaria_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //3
    path = [[NSBundle mainBundle] pathForResource:@"Falling_Glass_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //4
    path = [[NSBundle mainBundle] pathForResource:@"Herrera_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //5
    path = [[NSBundle mainBundle] pathForResource:@"Jones_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //6
    path = [[NSBundle mainBundle] pathForResource:@"Mata_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //7
    path = [[NSBundle mainBundle] pathForResource:@"Shaw_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //8
    path = [[NSBundle mainBundle] pathForResource:@"Spinning_Trident_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //9
    path = [[NSBundle mainBundle] pathForResource:@"Title_Sequence_YouTube CLEAN" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //10
    path = [[NSBundle mainBundle] pathForResource:@"Title_Sequence_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //11
    path = [[NSBundle mainBundle] pathForResource:@"Trident_Grip_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //12
    path = [[NSBundle mainBundle] pathForResource:@"Walking_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //13
    path = [[NSBundle mainBundle] pathForResource:@"Wilson_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    //14
    path = [[NSBundle mainBundle] pathForResource:@"Young_YouTube" ofType:@"mp4" inDirectory:@""];
    file = [NSURL fileURLWithPath:path];
    
    item = [AVPlayerItem playerItemWithURL:file];
    player = [AVPlayer playerWithPlayerItem:item];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [playlist addObject:player];
    
    
    
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

@end
