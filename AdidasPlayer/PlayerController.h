//
//  PlayerController.h
//  AdidasPlayer
//
//  Created by Radek Zmeskal on 22/07/15.
//  Copyright (c) 2015 Radek Zmeskal. All rights reserved.
//

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PlayerController : AVPlayerViewController
{
    NSInteger index;
    
    NSMutableArray *playlist;
}


@end
